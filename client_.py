import requests
import json

body = {
    "acao": "multiplicar",
    "valorA": 3,
    "calorB":3
    }

body = json.dumps(body)

headers = {'Content-Type': 'application/json'}

r = requests.post("http://localhost/calcular", data=body, headers=headers)

j = r.json()

print(j['resultado'])
